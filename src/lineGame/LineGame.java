package lineGame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import joy.tools;
import game.*;

public class LineGame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RunInfo(5000);
	}

	static void RunInfo(int round) {
		long time1, time2;
		time1 = System.currentTimeMillis();
		long mgScore = 0;
		long fgScore = 0;
		int trigger = 0;
		int totalBet = 9;
		Game.GameResult result = new Game.GameResult("MAIN");
		// 要塞ＤＢ的資料
		int[] db_Score = new int[round];
		String[] db_gameType = new String[round];

		for (int i = 0; i < round;) {
			if (result == null || result.Status == "MAIN") {
//				System.out.println("1");
				result = Game.GetMainGameInfo();
				mgScore += result.TotalWin;
//				System.out.println("2");
//				System.out.println("status:" + result.Status);
//				System.out.println("win:   " + result.TotalWin);
//				if (result.TotalWin > 0) {
//					for (int j = 0; j < result.Info[0].WinLineInfo.length; j++) {
//						System.out.println("info:  " + result.Info[0].WinLineInfo[j].WinPoint);
//					}
//				}
//				System.out.println("3");
				db_gameType[i] = "MAIN";
				if (result.Status == "FREE") {
					trigger++;
					db_gameType[i] = "FREE";
				}
				db_Score[i] += result.TotalWin;
				i++;
			} else if (result.Status == "FREE") {
				result = Game.GetFreeGameInfo(result);
				fgScore += result.TotalWin;
				db_Score[i - 1] += result.TotalWin;
			}
		}
		// 把資料塞進資料庫
		// 批次塞進
		ToDB(round,db_Score,db_gameType);

		time2 = System.currentTimeMillis();

		System.out.println("ALL_RTP:  " + (double) (mgScore + fgScore) / (double) round / (double) totalBet);
		System.out.println("MG_RTP:   " + (double) mgScore / (double) round / (double) totalBet);
		System.out.println("FG_RTP:   " + (double) fgScore / (double) round / (double) totalBet);
		System.out.println("Trigger:  " + (double) trigger / (double) round);
		System.out.println("====================");
		System.out.println("花費時間:   " + (time2 - time1) / (double) 1000 + " 秒");
	}

	static void ToDB(int dataCount,int[] score, String[] gameType) {
		final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/LineGame?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
		// 数据库的用户名与密码，需要根据自己的设置
		final String USER = "root";
		final String PASS = "12345678";
		final String EMP_ADD = 
				"INSERT INTO game(id,totalScore,gameType)"
				+ " VALUES (?,?,?)";
		Connection conn = null;
		PreparedStatement stmt = null;
	    
		try {
			// 注册 JDBC 驱动
			Class.forName(JDBC_DRIVER);

			// 打开链接
			System.out.println("连接数据库...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// 执行查询
			System.out.println(" 实例化Statement对象...");
			stmt = conn.prepareStatement(EMP_ADD);
			for(int i = 0; i < dataCount; i++){
				System.out.println(i);
				stmt.setInt(1, (i + 10000));
				stmt.setInt(2,score[i]);
				stmt.setString(3,gameType[i]);
				stmt.addBatch();
	        }
	        //  批次處理
			stmt.executeBatch();
			// 完成后关闭
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// 处理 JDBC 错误
			se.printStackTrace();
		} catch (Exception e) {
			// 处理 Class.forName 错误
			e.printStackTrace();
		} finally {
			// 关闭资源
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // 什么都不做
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Goodbye!");
	}
	
	// 查詢資料
		 static String SelectFromData() {
			String sql;
			sql = "SELECT id, name, url FROM websites";

			return sql;
		}
		
			
		 // 新增資料
		static String InsertData() {
			String sql;
			sql = "INSERT INTO `websites` VALUES ('8', 'YAHOO', 'https://www.yahoo.cm/', '10', 'JAP'),('9', 'SINA', 'https://www.sina.cm/', '100', 'JAP')";
			System.out.println(" 新增資料...");
			
			return sql;
		}

		 // 刪除資料
		static String DeleteData() {
			String sql;
			sql = "DELETE FROM websites WHERE id >= 7";
			System.out.println(" 刪除資料...");
			
			return sql;
		}

		 // 修改資料
		static String UpdateData() {
			String sql;
			sql = "UPDATE websites SET name = 'YAHOO' WHERE id=6";
			System.out.println(" 修改資料...");
			
			return sql;
		}

}
