package game;

import joy.tools;
import java.util.*;

public class Game {
	// 總結果
	public static class GameResult {
		public int TotalWin;
		public String Status;
		public GameInfo[] Info;
		public GameResult(){};
		public GameResult(String str){
			this.Status=str;
		};
	}

	// 每局結果
	public static class GameInfo {
		public int[] RNG;
		public int[][] Reel;
		public  LineInfo[] WinLineInfo;
		public SCLineInfo ScatterInfo;
	}

	// 連線資訊
	public static class LineInfo {
		public int Symbol;
		public int WinPoint;
		public int Count;
		public int LineIndex;
	}

	public static class SCLineInfo {
		public int Symbol;
		public int WinPoint;
		public int Count;
	}
	
	final static int WildNumber = 12;
	final static int ScatterNumber = 13;
	final static int TotalBet = 9;
	static int[][] mainGameReel = {
			{ 3, 6, 9, 5, 8, 10, 3, 6, 7, 12, 10, 7, 12, 10, 6, 1, 10, 6, 2, 8, 6, 2, 8, 10, 3, 7, 9, 1, 6, 10, 2, 7,
					10, 4, 9, 10, 5, 6, 10, 4, 9, 6, 1, 8, 6, 5, 10, 9, 4, 8, 9, 3, 6, 8, 5, 7, 10 },
			{ 1, 6, 10, 1, 7, 6, 1, 7, 9, 5, 10, 7, 4, 9, 7, 5, 6, 9, 1, 8, 9, 3, 6, 10, 5, 6, 9, 5, 8, 9, 12, 6, 9, 3,
					6, 9, 3, 6, 8, 4, 6, 10, 12, 6, 7, 5, 9, 10, 2, 8, 10, 4, 9, 10, 2, 9, 10 },
			{ 1, 8, 10, 1, 9, 10, 3, 6, 8, 12, 10, 8, 4, 9, 8, 13, 10, 8, 1, 7, 10, 1, 9, 8, 13, 10, 8, 4, 6, 10, 5, 9,
					10, 13, 8, 6, 12, 10, 7, 2, 8, 10, 2, 8, 9, 3, 10, 8, 5, 7, 8, 1, 9, 8, 5, 7, 10, 12, 6, 10, 1, 7,
					9, 3, 10, 7, 2, 8, 6, 4, 10, 7, 5, 10, 9, 13, 6, 7, 13, 10, 6 },
			{ 2, 9, 7, 3, 8, 6, 4, 6, 7, 5, 6, 7, 5, 9, 7, 4, 7, 10, 3, 6, 7, 5, 8, 9, 3, 7, 9, 4, 7, 10, 12, 6, 8, 13,
					7, 10, 13, 7, 6, 2, 7, 9, 13, 7, 9, 4, 7, 9, 13, 7, 9, 12, 9, 8, 1, 9, 8, 13, 9, 7, 5, 9, 8, 3, 9,
					8 },
			{ 4, 7, 1, 13, 7, 2, 13, 9, 3, 13, 6, 4, 13, 8, 5, 13, 7, 10, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 7, 6, 5, 7, 8,
					3, 9, 10, 4, 6, 10, 5, 6, 7, 5, 8, 7, 3, 10, 6, 12, 8, 9, 3, 8, 7, 12, 10, 7, 4, 8, 7, 4, 10, 8, 3,
					10, 9, 5, 7, 8 }, };
	static int[][] freeGameReel = {
			{ 1, 5, 5, 1, 4, 4, 1, 3, 3, 1, 5, 5, 1, 3, 3, 1, 5, 5, 1, 5, 5, 2, 3, 3, 2, 4, 4, 1, 5, 5, 1, 3, 3, 1, 5,
					5, 12, 3, 3, 2, 5, 5, 1, 3, 3 },
			{ 2, 4, 4, 2, 3, 3, 2, 4, 4, 2, 3, 3, 2, 4, 4, 2, 3, 3, 1, 4, 4, 2, 3, 3, 1, 4, 4, 2, 3, 3, 2, 4, 4, 2, 5,
					5, 12, 4, 4, 2, 3, 3, 2, 5, 5, 2, 4, 4, 2, 3, 3, 2, 4, 4, 2, 3, 3, 2, 4, 4, 2, 3, 3, 1, 4, 4, 2, 3,
					3, 1, 4, 4, 2, 3, 3, 2, 4, 4, 2, 5, 5 },
			{ 1, 4, 4, 2, 5, 5, 2, 4, 4, 1, 5, 5, 1, 4, 4, 2, 5, 5, 1, 4, 4, 2, 5, 5, 1, 4, 4, 2, 5, 5, 1, 4, 4, 2, 5,
					5, 12, 4, 4, 1, 3, 3, 2, 5, 5, 1, 4, 4, 2, 5, 5, 1, 4, 4, 2, 5, 5 },
			{ 1, 5, 5, 1, 3, 3, 1, 5, 5, 1, 3, 3, 1, 5, 5, 1, 3, 3, 2, 5, 5, 1, 3, 3, 2, 5, 5, 1, 3, 3, 1, 5, 5, 1, 4,
					4, 12, 5, 5, 1, 3, 3, 1, 4, 4, 1, 5, 5, 1, 3, 3, 1, 5, 5, 1, 3, 3, 1, 5, 5, 1, 3, 3, 2, 5, 5, 1, 3,
					3, 2, 5, 5, 1, 3, 3, 1, 5, 5, 1, 4, 4 },
			{ 2, 4, 4, 2, 5, 5, 2, 3, 3, 2, 4, 4, 2, 3, 3, 2, 4, 4, 2, 4, 4, 1, 3, 3, 1, 5, 5, 2, 4, 4, 2, 3, 3, 2, 4,
					4, 12, 3, 3, 1, 4, 4, 2, 3, 3 }, };
	static int[][] payTable = { /* 0 */ /* 無 */ { 0, 0, 0, 0, 0, 0 }, /* 1 */ /* M1 */ { 0, 0, 0, 88, 188, 1088 },
			/* 2 */ /* M2 */ { 0, 0, 0, 88, 188, 1088 }, /* 3 */ /* M3 */ { 0, 0, 0, 48, 168, 388 },
			/* 4 */ /* M4 */ { 0, 0, 0, 38, 108, 288 }, /* 5 */ /* M5 */ { 0, 0, 0, 28, 88, 188 },
			/* 6 */ /* A */ { 0, 0, 0, 18, 48, 88 }, /* 7 */ /* K */ { 0, 0, 0, 18, 48, 88 },
			/* 8 */ /* Q */ { 0, 0, 0, 8, 38, 68 }, /* 9 */ /* J */ { 0, 0, 0, 8, 38, 68 },
			/* 10 */ /* TE */ { 0, 0, 0, 8, 38, 68 }, /* 11 */ /* 無 */ { 0, 0, 0, 0, 0, 0 },
			/* 12 */ /* WW */ { 0, 0, 0, 0, 0, 0 }, /* 13 */ /* SC */ { 0, 0, 0, 2, 0, 0 }, };
	static int[][] mgLinePos = {
			// 1 ~ 5
			{ 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0 }, { 2, 2, 2, 2, 2 }, { 0, 1, 2, 1, 0 }, { 2, 1, 0, 1, 2 },
			// 6 ~ 9
			{ 1, 0, 0, 0, 1 }, { 1, 2, 2, 2, 1 }, { 0, 0, 1, 2, 2 }, { 2, 2, 1, 0, 0 }, 
			};
	static int[][] fgLinePos = {
			// 1 ~ 5
			{ 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0 }, { 2, 2, 2, 2, 2 }, { 0, 1, 2, 1, 0 }, { 2, 1, 0, 1, 2 },
			// 6 ~ 9
			{ 1, 0, 0, 0, 1 }, { 1, 2, 2, 2, 1 }, { 0, 0, 1, 2, 2 }, { 2, 2, 1, 0, 0 }, };
	static int[] freeGameRound = { 0, 0, 0, 8, 0, 0 };

	// mg_spin
	public static GameResult GetMainGameInfo() {
		GameResult result = new GameResult("MAIN");
		List<GameInfo> info = new ArrayList<GameInfo>();
		GameInfo tmp = new GameInfo();

		int[] rngResult = GetOneRNGforEveryReelForMainGame();
		int[][] reelResult = GetNotSortSymbolsForMainGame(rngResult);
//		int[][] reelResult= {{1,1,9},{1,1,5},{1,1,2},{1,1,5},{1,1,2}};
		tmp.RNG = rngResult;
		tmp.Reel = reelResult;
		tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, mgLinePos);
		tmp.ScatterInfo = GetScatterInfo(tmp.Reel);

		info.add(tmp);

		result.Status = "MAIN";
		if (tmp.ScatterInfo.Count >= 3) {
			result.Status = "FREE";
		}
		result.Info = info.toArray(new GameInfo[info.size()]);
		result.TotalWin = GetTotalWin(result.Info);
		return result;
	}

	// fg_spin
	public static GameResult GetFreeGameInfo(GameResult lastResult) {
		GameResult result = new GameResult("FREE");
		List<GameInfo> info = new ArrayList<GameInfo>();
		int fs = freeGameRound[lastResult.Info[0].ScatterInfo.Count];
		for (; fs > 0; fs--) {
			GameInfo tmp = new GameInfo();
			int[] rngResult = GetOneRNGforEveryReelForFreeGame();
			int[][] reelResult = GetNotSortSymbolsForFreeGame(rngResult);
			tmp.RNG = rngResult;
			tmp.Reel = reelResult;
			tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, fgLinePos);
			tmp.ScatterInfo = GetScatterInfo(tmp.Reel);

			info.add(tmp);

			if (tmp.ScatterInfo.Count >= 3) {
				fs += freeGameRound[lastResult.Info[0].ScatterInfo.Count];
			}
		}
		result.Status = "MAIN";
		result.Info = info.toArray(new GameInfo[info.size()]);
		result.TotalWin = GetTotalWin(result.Info);
		return result;
	}

	private static int[] GetOneRNGforEveryReelForMainGame() {
		int[] result = new int[5];

		for (int index = 0; index < result.length; index++) {
			result[index] = tools.getRandom_Int(mainGameReel[index].length);
		}
		return result;
	}

	private static int[] GetOneRNGforEveryReelForFreeGame() {
		int[] result = new int[5];

		for (int index = 0; index < result.length; index++) {
			result[index] = tools.getRandom_Int(freeGameReel[index].length);
		}
		return result;
	}

	private static int[][] GetNotSortSymbolsForMainGame(int[] rng) {
		int[][] result = new int[5][];
		for (int reel = 0; reel < result.length; reel++) {
			result[reel] = GetSingleReelSymbol(mainGameReel[reel], 3, rng[reel]);
		}
		return result;
	}

	private static int[][] GetNotSortSymbolsForFreeGame(int[] rng) {
		int[][] result = new int[5][];
		for (int reel = 0; reel < result.length; reel++) {
			result[reel] = GetSingleReelSymbol(freeGameReel[reel], 3, rng[reel]);
		}
		return result;
	}

	private static int[] GetSingleReelSymbol(int[] gameReel, int columnCount, int rng) {
		int[] result = new int[3];
		for (int col = 0; col < result.length; col++) {
			result[col] = gameReel[(rng + col) % (gameReel.length)];
		}
		return result;

//		List<Integer> result= new ArrayList<Integer>();
//		
//		return result.stream()
//                .mapToInt(Integer::intValue)
//                .toArray();
	}

	private static LineInfo[] GetWinLineInfo(int[][] reel, int[][] linePos) {
		List<LineInfo> result = new ArrayList<LineInfo>();
		for (int lineIndex = 0; lineIndex < linePos.length; lineIndex++) {
			LineInfo tmp = new LineInfo();
			int[] tmpLineIndex = linePos[lineIndex];
			if (reel[0][tmpLineIndex[0]] != WildNumber) {
				tmp = SymbolLineInfo(reel, tmpLineIndex, reel[0][tmpLineIndex[0]]);
				tmp.LineIndex = lineIndex;
			} else {
				// wild 開頭
				tmp = WildLineInfo(reel, tmpLineIndex);
				tmp.LineIndex = lineIndex;
			}
			if (tmp.WinPoint > 0 && tmp.Symbol != ScatterNumber) {
				result.add(tmp);
			}
		}
		if (result.size() > 0) {
			return result.toArray(new LineInfo[result.size()]);
		}
		return null;

	}

	private static LineInfo SymbolLineInfo(int[][] reel, int[] linePos, int targetSymbol) {
		LineInfo result = new LineInfo();
		int rowIndex = 1;
		for (; rowIndex < linePos.length;) {
			if (reel[rowIndex][linePos[rowIndex]] == targetSymbol || reel[rowIndex][linePos[rowIndex]] == WildNumber) {
				rowIndex++;
			} else {
				break;
			}
		}
		result.Count = rowIndex;
		result.Symbol = targetSymbol;
		result.WinPoint = payTable[result.Symbol][result.Count];
		return result;
	}

	private static LineInfo WildLineInfo(int[][] reel, int[] linePos) {
		// 純粹wild
		LineInfo onlyWild = new LineInfo();
		int rowIndex = 1;
		int nowSymbol = WildNumber;
		for (; rowIndex < linePos.length;) {
			if (reel[rowIndex][linePos[rowIndex]] == nowSymbol) {
				rowIndex++;
			} else {
				break;
			}
		}
		onlyWild.Count = rowIndex;
		onlyWild.Symbol = nowSymbol;
		onlyWild.WinPoint = payTable[onlyWild.Symbol][onlyWild.Count];

		// 混搭
		LineInfo normal = new LineInfo();
		rowIndex = 1;
		for (; rowIndex < linePos.length;) {
			if (nowSymbol != WildNumber && reel[rowIndex][linePos[rowIndex]] != nowSymbol
					&& reel[rowIndex][linePos[rowIndex]] != WildNumber) {
				break;
			} else {
				if (nowSymbol == WildNumber && reel[rowIndex][linePos[rowIndex]] != WildNumber) {
					nowSymbol = reel[rowIndex][linePos[rowIndex]];
				}
				rowIndex++;
			}
		}
		normal.Count = rowIndex;
		normal.Symbol = nowSymbol;
		normal.WinPoint = payTable[normal.Symbol][normal.Count];

		if (onlyWild.WinPoint > normal.WinPoint) {
			return onlyWild;
		}
		return normal;
	}

	private static SCLineInfo GetScatterInfo(int[][] reel) {
		SCLineInfo scInfo = new SCLineInfo();
		int count = 0;

		for (int r = 0; r < reel.length; r++) {
			for (int c = 0; c < reel[r].length; c++) {
				if (reel[r][c] == ScatterNumber) {
					count++;
				}
			}
		}
		scInfo.Symbol = ScatterNumber;
		scInfo.Count = count;
		scInfo.WinPoint = payTable[scInfo.Symbol][scInfo.Count] * TotalBet;
		return scInfo;
	}

	private static int GetTotalWin(GameInfo[] info) {
		int result = 0;
		for (int i = 0; i < info.length&&info!=null; i++) {
			if (info[i].WinLineInfo==null) {
				continue;
			}
			for (int wl = 0; wl < info[i].WinLineInfo.length; wl++) {
				result += info[i].WinLineInfo[wl].WinPoint;
			}
			result += info[i].ScatterInfo.WinPoint;
		}
		return result;
	}
}


